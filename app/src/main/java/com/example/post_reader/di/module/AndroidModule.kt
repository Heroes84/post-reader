package com.example.post_reader.di.module

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class AndroidModule {

    @Provides
    fun providesContext(application: Application) = application.baseContext

}