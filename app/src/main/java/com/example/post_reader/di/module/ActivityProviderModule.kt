package com.example.post_reader.di.module

import com.example.post_reader.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityProviderModule {

    @ContributesAndroidInjector()
    abstract fun contributeMainActivity(): MainActivity
}
