package com.example.post_reader.di.module

import com.example.post_reader.presentation.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentProviderModule {

    @ContributesAndroidInjector
    abstract fun provideDesktopFragment(): ListFragment
}