package com.example.post_reader.presentation

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.post_reader.R
import com.example.post_reader.domain.model.Post

class PostsViewHolder(
    val view: View
) : RecyclerView.ViewHolder(view) {

    fun bind(post: Post) {
        view.findViewById<TextView>(R.id.textview_title).setText(post.title)
    }
}